**This repository contains wrappers for the mapping tool bwa to be run in a Galaxy environment.**


**Wrappers** folder contains the XML wrapper and the associated python.


**data.test** folder contains several datasets to use together to test the tool in Galaxy : 

    ref2.fa and reads2.bam 


    ref.fa and fastq (for the single-end reads mode)


    ref.fa, left.fastq and right.fastq (for the paired-end reads mode)


**manuals** folder contains : 
- the admin manual which discribes how to install the tool in your own Galaxy instance 
- the user manual which explains how the tool works