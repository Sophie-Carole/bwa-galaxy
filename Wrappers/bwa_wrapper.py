import argparse, os, subprocess, sys, tempfile, shutil

"""
Programme en python 3.7
Permet de lancer bwa-0.7.17
Fournit un fichier au format SAM 

Utilisation : python bwa_wrapper.py [arguments]

Auteurs : Tomas Caetano, Sophie-Carole Chobert, Valentine Delay
"""


"""

Parsing des arguments 

"""

parser = argparse.ArgumentParser()

#Arguments type fichiers
parser.add_argument('--fichier', required=True, help ="Reference genome")
parser.add_argument('--fastq', required=True)
parser.add_argument('--chemin', required= True, dest="pw")
parser.add_argument('--output', required=True, dest="output")

#Argument paired-end reads
parser.add_argument('--fastq2', required=False, help = "If there are paired-end reads")

#Arguments index
parser.add_argument('--index_param', required=False,help="Algorithm used to build index by default (IS = the fastest)")

#Arguments aln
parser.add_argument('--Max_frac', required=False, help = "Maximum edit distance")
parser.add_argument('--opening', required=False, help = "Maximum number of gap opens")
parser.add_argument('--open_penalty', required=False, help = "Gap opening penalty")
parser.add_argument('--extension', required=False, help = "Maximum number of gap extensions")
parser.add_argument('--exten_penalty', required=False, help = "Gap extension penalty")
parser.add_argument('--mismatch_penalty', required=False, help = "Mismatch penalty")
parser.add_argument('--threads', required=False, help = "Number of threads")
parser.add_argument('--disallow_del', required=False, help = "Disallow long deletion within the provided value in bp towards the 3'-end")
parser.add_argument('--disallow_indel', required=False, help = "Disallow an indel within the provided value in bp towards the ends")
parser.add_argument('--subseq_seed', required=False, help = "Select the number of subsequences to take as seed")
parser.add_argument('--maxdist_seed', required=False, help = "Maximum edit distance in the seed")
parser.add_argument('--suboptimal', required=False, help = "If there are no more than the provided value of equally best hits, proceed with suboptimal alignments")
parser.add_argument('--disable_iterative', required=False, help = "Disable iterative search")
parser.add_argument('--trimming', required=False, help = "Parameter for read trimming")
parser.add_argument('--len_barcode', required=False, help = "Length of barcode starting from the 5’-end")

#Arguments sampe + samse
parser.add_argument('-a',required=False, help = "Maximum insert size for a read pair to be considered being mapped properly") #sampe
parser.add_argument('-o',required=False, help = "Maximum occurrences of a read for pairing") #sampe
parser.add_argument('-n',required=False, help = "Maximum number of alignments to output in the XA tag for reads paired properly") #sampe + samse

#Arguments BAM :
parser.add_argument('-b0',required=False)
parser.add_argument('-b1',required=False)
parser.add_argument('-b2',required=False)

args = parser.parse_args()

#Récupération du chemin de bwa_wrapper.py pour l'accession au programme bwa
position = args.pw
bwa = position+"/./bwa"


#Code pour les différentes commandes de BWA
#Récupération arguments
fastq_1 = args.fastq
if args.fastq2 :
    fastq_2 = args.fastq2
sam_outfile = args.output

#index
algo_index = args.index_param

#aln
max_fr = args.Max_frac
opening_option = args.opening
open_pen = args.open_penalty
extend = args.extension
extend_pen = args.exten_penalty
mismatch_pen = args.mismatch_penalty
thr=args.threads
dis_del = args.disallow_del
dis_ind = args.disallow_indel
sub_seed = args.subseq_seed
mdist_seed = args.maxdist_seed
subop = args.suboptimal
dis_it = args.disable_iterative
trim = args.trimming
l_barc = args.len_barcode 

#sampe/samse
insert_size = args.a
max_occur = args.o
max_align = args.n


"""

Création des dossiers temporaires et fichiers temporaires 

"""

dossier_temp = tempfile.mkdtemp()

alignement_temp = tempfile.NamedTemporaryFile( dir=dossier_temp )
alignement_temp_name = alignement_temp.name
alignement_temp.close()

alignement_temp2 = tempfile.NamedTemporaryFile( dir=dossier_temp )
alignement_temp2_name = alignement_temp2.name
alignement_temp2.close()

ref_genome = tempfile.NamedTemporaryFile( dir=dossier_temp )
ref_genome_name = ref_genome.name
ref_genome.close()



"""

Définitition des variables pour la commande bwa 

"""

# Indexation du génome de référence choisi

os.symlink (args.fichier, ref_genome_name)
index = '%s index -a %s %s' % (bwa, algo_index, ref_genome_name)

options_aln = ''; option_bam_1 =''; option_bam_2 =''; options_sam = ''; option_bam =''


# aln / samse ou sampe 

if args.Max_frac: #si plus d'options a été sélectionné par l'utilisateur
    options_aln = '-n %s -o %s -O %s -e %s -d %s -i %s -l %s -k %s -E %s -R %s -t %s -M %s -q %s -B %s' % (max_fr, opening_option, open_pen, extend, dis_del, dis_ind, sub_seed, mdist_seed, extend_pen, subop, thr, mismatch_pen, trim, l_barc)
    
if args.disable_iterative:
    options_aln += '-N'
    
if args.b0: # si input est un fichier bam
    option_bam = '-b0' 
    
if args.fastq2 : # si les reads sont paired ends
    if args.b1: # si input sont des fichiers bam
        option_bam_1 = '-b1' 
        option_bam_2 = '-b2' 
        
    options_sam = '-a %s -o %s -n %s' % (insert_size, max_occur, max_align)
    aln1 = '%s aln %s %s %s %s > %s' % (bwa, options_aln, ref_genome_name, option_bam_1, fastq_1, alignement_temp_name)
    aln2 = '%s aln %s %s %s %s > %s' % (bwa, options_aln, ref_genome_name, option_bam_2, fastq_2, alignement_temp2_name)
    sampe = '%s sampe %s %s %s %s %s %s > %s' % (bwa, options_sam, ref_genome_name, alignement_temp_name, alignement_temp2_name, fastq_1, fastq_2, sam_outfile)

else: # si single end 
    options_sam = '-n %s' % (max_align)
    aln1 = '%s aln %s %s %s %s > %s' % (bwa, options_aln, ref_genome_name, option_bam, fastq_1, alignement_temp_name)
    samse = '%s samse %s %s %s %s > %s' % (bwa, options_sam, ref_genome_name, alignement_temp_name, fastq_1, sam_outfile)



"""

Lancement du processus

"""

temp = tempfile.NamedTemporaryFile(dir=dossier_temp).name
std_out = open( temp, 'wb' )
p=subprocess.Popen(args = index, shell=True, cwd=dossier_temp,stderr=std_out.fileno())
code=p.wait()
std_out.close()

tmp = tempfile.NamedTemporaryFile(dir=dossier_temp).name
std_out = open( temp, 'wb' )
p=subprocess.Popen(args = aln1, shell=True, cwd=dossier_temp,stderr=std_out.fileno())
code=p.wait()
std_out.close()

if args.fastq2 :
    temp = tempfile.NamedTemporaryFile( dir=dossier_temp ).name
    std_out = open( temp, 'wb' )
    p=subprocess.Popen(args = aln2, shell=True, cwd=dossier_temp, stderr=std_out.fileno())
    code=p.wait()
    std_out.close()

    temp = tempfile.NamedTemporaryFile( dir=dossier_temp ).name
    std_out = open( temp, 'wb' )
    p=subprocess.Popen(args = sampe, shell=True, cwd=dossier_temp, stderr=std_out.fileno())
    code=p.wait()
    std_out.close()

else:
    temp = tempfile.NamedTemporaryFile( dir=dossier_temp ).name
    std_out = open( temp, 'wb' )
    p=subprocess.Popen(args = samse, shell=True,cwd=dossier_temp,stderr=std_out.fileno())
    p.wait()
    std_out.close()


if os.path.exists( dossier_temp ):
    shutil.rmtree( dossier_temp )
